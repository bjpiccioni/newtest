EESchema Schematic File Version 5
EELAYER 31 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 00000000
P 3925 3600
F 0 "R1" H 3995 3645 50  0000 L CNN
F 1 "R" H 3995 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1020_2550Metric" V 3855 3600 50  0001 C CNN
F 3 "~" H 3925 3600 50  0001 C CNN
	1    3925 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 00000000
P 4175 3600
F 0 "R2" H 4245 3645 50  0000 L CNN
F 1 "R" H 4245 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1020_2550Metric" V 4105 3600 50  0001 C CNN
F 3 "~" H 4175 3600 50  0001 C CNN
	1    4175 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 00000000
P 4450 3600
F 0 "R3" H 4520 3645 50  0000 L CNN
F 1 "R" H 4520 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1020_2550Metric" V 4380 3600 50  0001 C CNN
F 3 "~" H 4450 3600 50  0001 C CNN
	1    4450 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
